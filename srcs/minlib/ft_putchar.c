//
// Created by lune on 2/25/17.
//

#include "minlib.h"

static char	shift(unsigned char *str)
{
	char i;
	char len;

	len = 4;
	while (str[0] == 0)
	{
		i = 1;
		while (i < 4)
		{
			str[i - 1] = str[i];
			i++;
		}
		len--;
	}
	while (str[i])
		i++;
	return (i < len ? i : len);
}

int ft_putchar(int ch)
{
	unsigned char	symb[4];
	char			i;

	symb[0] = (unsigned char)(ch >> 24);
	symb[1] = (unsigned char)(ch >> 16);
	symb[2] = (unsigned char)(ch >> 8);
	symb[3] = (unsigned char)(ch);
	i = shift(symb);
	write(1, symb, (size_t)i);
	return (i);
}