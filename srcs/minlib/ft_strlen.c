//
// Created by Serhii Petrenko on 2/15/17.
//

#include "../../includes/minlib.h"

size_t	ft_strlen(const char *s)
{
	size_t i;

	i = 0;
	while (s && s[i])
		i++;
	return (i);
}

size_t	ft_wstrlen(const wint_t *s)
{
	size_t i;

	i = 0;
	while (s && s[i])
		i++;
	return (i);
}