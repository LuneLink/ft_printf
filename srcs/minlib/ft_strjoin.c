//
// Created by Serhii Petrenko on 2/16/17.
//

#include "../../includes/minlib.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char *ret_str;

	if (!s1 || !s2)
		return (0);
	ret_str = (char *)ft_memalloc(ft_strlen(s1) + ft_strlen(s2) + 1);
	if (ret_str)
	{
		ft_strcat(ret_str, s1);
		ft_strcat(ret_str, s2);
	}
	return (ret_str);
}