//
// Created by Serhii Petrenko on 2/27/17.
//

#include "minlib.h"

void	*ft_memcpy(void *destination, const void *source, size_t count)
{
	size_t i;

	i = -1;
	while (++i < count)
		((char *)destination)[i] = ((char*)source)[i];
	return (destination);
}