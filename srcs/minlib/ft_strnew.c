/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: spetrenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 17:41:45 by spetrenk          #+#    #+#             */
/*   Updated: 2016/11/30 09:28:51 by spetrenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minlib.h"

char	*ft_strnew(size_t size)
{
	return (char*)ft_memalloc(size + 1);
}

wint_t	*ft_wstrnew(size_t size)
{
	return  (wint_t *)ft_memalloc((size + 1) * sizeof(wint_t));
}