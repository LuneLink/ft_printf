//
// Created by Serhii Petrenko on 2/16/17.
//

#include "../../includes/minlib.h"

void	add_and_nextb(wint_t **str, char symb, size_t count)
{
	if (symb)
		while (count)
		{
			**str = symb;
			(*str)++;
			count--;
		}
}

void	add_and_nextbs(wint_t **str, wint_t *str2)
{
	while (str2 && *str2)
	{
		**str = (*str2);
		(*str)++;
		str2++;
	}
}

void	add_and_nexts(wint_t **str, char symb, size_t count)
{
	if (symb)
		while (count > 0)
		{
			**str = symb;
			(*str)--;
			count--;
		}
}

void	add_and_nextss(wint_t **str, wint_t *str2)
{
	size_t i;

	i = 0;
	if (!str2)
		return ;
	while (*str2)
	{
		str2++;
		i++;
	}
	str2--;
	while (i > 0)
	{
		**str = *str2;
		(*str)--;
		str2--;
		i--;
	}
}