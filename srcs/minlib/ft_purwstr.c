//
// Created by Serhii Petrenko on 2/28/17.
//

#include "minlib.h"

int 	ft_putwstr(wint_t *str)
{
	int i;

	i = 0;
	while (*str)
	{
		i += ft_putchar(*str);
		str++;
	}
	return (i);
}