//
// Created by Serhii Petrenko on 2/22/17.
//

#include "minlib.h"

static  size_t get_numb_count(intmax_t numb, char base)
{
	size_t i;

	i = 1;
	if (numb != 0)
	{
		i--;
		while (numb > 0)
		{
			numb /= base;
			i++;
		}
	}
	return (i);
}
static  size_t get_unumb_count(uintmax_t numb, char base)
{
	size_t i;

	i = 1;
	if (numb != 0)
	{
		i--;
		while (numb > 0)
		{
			numb /= base;
			i++;
		}
	}
	return (i);
}

static int	digit_count(wint_t **str, intmax_t numb)
{
	int		i;

	i = 0;
	if (numb == 0)
	{
		(*str) = ft_wstrnew(1);
		(*str)[0] = '0';
	}
	else
	{
		if (numb < 0)
			i++;
		while (numb)
		{
			numb = numb / 10;
			i++;
		}
		(*str) = ft_wstrnew(i);
	}
	return (i);
}

char *get_hex_by_case(char up)
{
	static char *hex_up = "0123456789ABCDEF";
	static char *hex_low = "0123456789abcdef";

	if (up)
		return (hex_up);
	return (hex_low);
}

wint_t 	*uf_wuitoa_base(uintmax_t numb, char base, char up)
{
	wint_t	*str;
	char 	*hex;
	size_t	len;

	len = get_unumb_count(numb, base);
	hex = get_hex_by_case(up);
	str = ft_wstrnew(len);
	len--;
	if (numb == 0)
		str[len] = '0';
	else
		while (numb > 0)
		{
			str[len--] = hex[numb % base];
			numb /= base;
		}
	return (str);
}

wint_t		*ft_witoa(intmax_t n)
{
	int			size;
	wint_t		*ret_str;
	intmax_t	rem;

	size = digit_count(&ret_str, n);
	if (ret_str)
	{
		if (n == 0)
			return (ret_str);
		if (n < 0)
			ret_str[0] = '-';
		while (n != 0)
		{
			rem = n % 10;
			if (rem < 0)
				rem *= -1;
			ret_str[size - 1] = (wint_t)rem + '0';
			n = n / 10;
			size--;
		}
	}
	return (ret_str);
}
