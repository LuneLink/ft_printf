//
// Created by Serhii Petrenko on 2/15/17.
//

#include "../../includes/minlib.h"

char	*ft_strcpy(char *destination, const char *source)
{
	char *start_dest;

	start_dest = destination;
	while (*source)
		*(destination++) = *(source++);
	*(destination) = *(source);
	return (start_dest);
}