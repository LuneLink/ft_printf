//
// Created by Serhii Petrenko on 2/16/17.
//

#include "../../includes/minlib.h"

char	*ft_strcat(char *destination, const char *source)
{
	char *ptr_dest;

	ptr_dest = destination;
	while (*destination)
		destination++;
	while (*source)
		*(destination++) = *(source++);
	*destination = 0;
	return (ptr_dest);
}