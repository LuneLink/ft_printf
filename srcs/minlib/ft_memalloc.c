//
// Created by Serhii Petrenko on 2/15/17.
//

#include "../../includes/minlib.h"

void	*ft_memalloc(size_t size)
{
	size_t	i;
	char	*mem;

	mem = (char *)malloc(size);
	if (mem)
	{
		i = 0;
		while (i < size)
			mem[i++] = 0;
	}
	return (mem);
}
