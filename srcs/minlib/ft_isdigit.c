//
// Created by Serhii Petrenko on 2/20/17.
//

#include "minlib.h"

int	ft_isdigit(int ch)
{
	if (ch > 47 && ch < 58)
		return (1);
	return (0);
}