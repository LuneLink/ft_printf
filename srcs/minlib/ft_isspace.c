//
// Created by Serhii Petrenko on 2/20/17.
//

#include "minlib.h"

int		ft_isspace(int ch)
{
	if (ch == 32 || (ch > 8 && ch < 14))
		return (1);
	return (0);
}