#include <stdio.h>
#include "../includes/ft_printf.h"
#include <stddef.h>
#include <locale.h>
#include <limits.h>
#include <errno.h>
#include <stdio.h>
#include <stddef.h>
#include <locale.h>
#include <limits.h>
#include <errno.h>

/*void out_token(t_tokens *tokens)
{
	printf("flag -> %s\n",tokens->flag);
	printf("width -> %s\n",tokens->width);
	printf("precision -> %s\n",tokens->precision);
	printf("length -> %s\n",tokens->len);
	printf("specifier -> %c\n",tokens->specifier);
}*/

void test_mind()
{
	printf("%-+010d\n", 10);
	ft_printf("%-+010d\n", 10);
	printf("%-+010.3d\n", 10);
	ft_printf("%-+010.3d\n", 10);

	printf("%- 010d\n", 10);
	ft_printf("%- 010d\n", 10);
	printf("%- 010.3d\n", 10);
	ft_printf("%- 010.3d\n", 10);

	printf("%-+010d\n", -10);
	ft_printf("%-+010d\n", -10);
	printf("%-+010.3d\n", -10);
	ft_printf("%-+010.3d\n", -10);

	printf("%- 010d\n", -10);
	ft_printf("%- 010d\n", -10);
	printf("%- 010.3d\n", -10);
	ft_printf("%- 010.3d\n", -10);

	printf("%-010d\n", -10);
	ft_printf("%-010d\n", -10);
	printf("%-010.3d\n", -10);
	ft_printf("%-010.3d\n", -10);

	printf("%-010.70d\n", -10);
	ft_printf("%-010.70d\n", -10);
	printf("%-010.70d\n", 10);
	ft_printf("%-010.70d\n", 10);

	printf("%- 010.70d\n", 10);
	ft_printf("%- 010.70d\n", 10);
}

void test_d()
{

	printf("%+010d\n", 10);
	ft_printf("%+010d\n", 10);
	printf("%+010.3d\n", 10);
	ft_printf("%+010.3d\n", 10);

	printf("% 010d\n", 10);
	ft_printf("% 010d\n", 10);
	printf("% 010.3d\n", 10);
	ft_printf("% 010.3d\n", 10);

	printf("%+010d\n", -10);
	ft_printf("%+010d\n", -10);
	printf("%+010.3d\n", -10);
	ft_printf("%+010.3d\n", -10);

	printf("% 010d\n", -10);
	ft_printf("% 010d\n", -10);
	printf("% 010.3d\n", -10);
	ft_printf("% 010.3d\n", -10);

	printf("%010d\n", -10);
	ft_printf("%010d\n", -10);
	printf("%010.3d\n", -10);
	ft_printf("%010.3d\n", -10);

	printf("%010.70d\n", -10);
	ft_printf("%010.70d\n", -10);
	printf("%010.70d\n", 10);
	ft_printf("%010.70d\n", 10);

	printf("% 010.70d\n", 10);
	ft_printf("% 010.70d\n", 10);
}

void test_rnd()
{
	printf("% 010.70hhd\n", 226);
	ft_printf("% 010.70hhd\n", 226);
	printf("%hhd\n", 226);
	ft_printf("%hhd\n", 226);
	printf("%Ld\n", 226);
	ft_printf("%Ld\n", 226);
	printf("%zd\n", -226);
	ft_printf("%zd\n", -226);
	printf("%d\n", -226);
	ft_printf("%d\n", -226);
	printf("%lld\n", 226);
	ft_printf("%lld\n", 226);
	printf("%lld\n", -226);
	ft_printf("%lld\n", -226);
	printf("%ld\n", -226);
	ft_printf("%ld\n", -226);
	printf("%hd\n", -226);
	ft_printf("%hd\n", -226);
	printf("%hhd\n", -226);
	ft_printf("%hhd\n", -226);
	printf("%zd\n", -226);
	ft_printf("%zd\n", -226);
	printf("%-+10.20zd\n", -226);
	ft_printf("%-+10.20zd\n", -226);
	printf("%512.6ld\n", 18446744073709551615);
	ft_printf("%512.6ld",18446744073709551615);
	printf("%+5l12-.6d\n",184467, 636);
	ft_printf("%+5l12-.6d\n",184467, 636);
	printf("%5.d\n",20);
	ft_printf("%5.d\n",20);


	printf("\n\n%5l12.6d\n");
	ft_printf("%5l12.6d");
	printf("%z10.40d\n", -226);
	ft_printf("%z10.40d\n", -226);
	printf("%5l12.6d\n", 18446744073709551615);
	ft_printf("%5l12.6d",18446744073709551615);
	 printf("%+5l12-.6d\n",184467);
	 printf("%+5l12-.6d\n",184467);
}

void	test_o(void)
{
	printf("%-#.5x\n", 1000);
	ft_printf("%-#.5x\n", 1000);
	printf("%-#10.5x\n", 1000);
	ft_printf("%-#10.5x\n", 1000);
	printf("%-#.5X\n", 1000);
	ft_printf("%-#.5X\n", 1000);
	printf("%-#10.5X\n", 1000);
	ft_printf("%-#10.5X\n", 1000);
	printf("%-o\n", 1000);
	ft_printf("%-o", 1000);
	printf("%-10.5o\n", 1000);
	ft_printf("%-10.5o", 1000);
	printf("%- 10.5o\n", 1000);
	ft_printf("%- 10.5o", 1000);
	printf("% 10.5o\n", 1000);
	ft_printf("% 10.5o", 1000);
}

void	test_c(void)
{
	//printf(L"%10lc\n", L'и');
	//printf(L"%10lc\n", L'\x03b1');
	//ft_printf("%-#.5x\n", 1000);

}

void 	test3(void)
{
	/*printf("%07.5u\n", 4235);
	ft_printf("%07.5U\n", 4235);
	printf("%12i\n", INT_MIN);
	ft_printf("%12i\n", INT_MIN);*/
	printf("%012i\n", -42);
	ft_printf("%012i\n", -42);
	printf("%-12i\n", -42);
	ft_printf("%-12i\n", -42);
	printf("%12u\n", 0);
	ft_printf("%12u\n", 0);
	printf("%20.8.12.-6d\n", 42);
	ft_printf("%20.8.12.-6d\n", 42);
	printf("%.i\n", 0);
	ft_printf("%.i\n", 0);
}

/*
 * lld ld D lD  - test
 *
 */

#define RED "\x1b[31m"
#define NORM "\x1b[0m"

void LAL(void)
{
	printf("fef");
}

int main2()
{
	//1110 0010 1001 1000 1011 1010

	const wchar_t B[] = L"ድመቶች ሰዎች አልወደውም.";
	printf("|%5.3S|",B);
	ft_printf("|%5.3S|",B);
	//const wchar_t *B = L"ድመቶች ሰዎች አልወደውም.";
//	printf("|%5.3ls|\t\t|%3.5ls|\t|%7ls|\t|%7.3ls|\n",B, B,B,L"");
	//ft_printf("|%5.3S|\t\t|%3.5S|\t|%7S|\t|%7.3S|\n",B, B,B,L"");

	//const char A[] = "this is tEEEEst!";
	//PRINTF("|%5.3s|\t\t|%3.5s|\t|%7s|\t|%7.3s|",A, A,A,NULL);
	//PRINTF("|%07.5s|\t|%02.5s|\t|%.10s|\t\t|%.5s|\t",A,A,A, "");
	//PRINTF("|%-7.5s|\t|%-2.5s|\t|%-.10s|\t\t|%-.5s|\t",A,A,A, "");
	//PRINTF("|%-07.5s|\t|%-02.5s|\t|%-020s|\t|%-0.5s|\t",A,A,A, "");
	//printf("|%S|\t|%S|\t|%C|\t|%S|",L"Wide sample string..",L"米",L'米',L"");
	//ft_printf("|%S|\t|%S|\t|%C|\t|%S|",L"Wide sample string..",L"米",L'米',L"");
	//ft_putchar(14850234);
	//printf("|%S|\t|%S|\t|%C|\t|%S|",L"Wide sample string..\n",L"米",L'米',L"");
	//ft_printf("|%S|\t|%S|\t|%C|\t|%S|",L"Wide sample string..",L"米",L'米',L"");
	//printf("|%S|",L"米");
	//ft_printf("|%S|",L"米");
	//ft_putchar(31859);
	//printf("|%07.5s|\n",A);
	//ft_printf("|%07.5s|\n",A);
	//ft_printf("|%s|", "Я");
	//ft_putchar(53423);
	//ft_printf("|%s|", "ПрэвЭд!");
	//ft_printf("|%s|", "Я!");
	// int i = 10;
	//printf("%20.30p\n",p_i);
	//ft_printf("%20.30p\n",p_i);
	//test3();
	//setlocale()
	//setlocale(LC_ALL, "en_US.UTF-8");
	//wint_t j = 14850234;
	//printf("%lc\n", j);
	//printf("%10Z\n", 97);
	//ft_printf("%10Z", 97);
	//printf("|%+3c|(%3d)\t|%+3c|(%3d)\t|%+c|(%3d)", a, a, a+5, a+5, a+10, a+10);
	//ft_printf("|%+3c|(%3d)\t|%+3c|(%3d)\t|%+c|(%3d)", a, a, a+5, a+5, a+10, a+10);
	//printf("|%+c|(%3d)",a+10, a+10);
	//ft_putchar(j);
	//ft_printf("%-.5lc", 14850234);
	//PRINTF("|%+3c|(%3d)\t|%+3c|(%3d)\t|%+c|(%3d)", a, a, a+5, a+5, a+10, a+10);


	//printf("|%5h hi|\t|%20l li|\n", SHRT_MIN,LONG_MIN);
	//ft_printf("|%5h hi|\t|%20l li|\n", SHRT_MIN,LONG_MIN);

	//PRINTF("|%5h h"CNV"|\t|%20l l"CNV"|", SHRT_MIN,LONG_MIN);
	//printf("|%#6u|\t|%#-12u|\t|%#u|\t\t|%#09u|\t|%#02u|\n",8400, 8400, 0, 8400, 8400);
	//ft_printf("|%#6u|\t|%#-12u|\t|%#u|\t\t|%#09u|\t|%#02u|\n",8400, 8400, 0, 8400, 8400);

	//printf("|%#6.7O|\t|%#-12.7O|\t|%#.7O|\t|%#09.7O|\t|%#02.7O|\n", 8400,8400,0,8400,8400);
	//ft_printf("|%#6.7O|\t|%#-12.7O|\t|%#.7O|\t|%#09.7O|\t|%#02.7O|\n", 8400,8400,0,8400,8400);
	//printf("|%5hhU|\t|%5hU|\t|%20lU|\t|%20llU|\t|%20jU|\t|%10zU|\t|%10U|\n", LONG_MAX,LONG_MAX,LONG_MAX,LONG_MAX,LONG_MAX,-2,LONG_MAX);
	//ft_printf("|%5hhU|\t|%5hU|\t|%20lU|\t|%20llU|\t|%20jU|\t|%10zU|\t|%10U|\n", LONG_MAX,LONG_MAX,LONG_MAX,LONG_MAX,LONG_MAX,-2,LONG_MAX);
	//PRINTF("|%5hh"CNV"|\t|%5h"CNV"|\t|%20l"CNV"|\t|%20ll"CNV"|\t|%20j"CNV"|\t|%10z"CNV"|\t|%10"CNV"|", LONG_MAX,LONG_MAX,LONG_MAX,LONG_MAX,LONG_MAX,-2,LONG_MAX);
	//PRINTF("|%#6.7"CNV"|\t|%#-12.7"CNV"|\t|%#.7"CNV"|\t|%#09.7"CNV"|\t|%#02.7"CNV"|", 8400,8400,0,8400,8400);
	//PRINTF("|%#6"CNV"|\t|%#-12"CNV"|\t|%#"CNV"|\t\t|%#09"CNV"|\t|%#02"CNV"|", 8400,8400,0,8400,8400);
	//226   152 186
	//int int_symb = ft_putchar(14850234);

	//char k = 'a';
	//wint_t j = 'ф';
	//putchar(j);

	//ft_putchar(j);
	//write(1, "\n", 1);
	//test_mind();
	//test_d();
	//test_rnd();
	//test_o();
	//test_c();
	//int nmb = 4235;
	//putchar(0xFFFFFFFF);
	//putchar(int_symb);
	//printf("\n|%8.5u|\t|%8.5u|\t|%08u|\t|%08u|\n",nmb, -nmb, nmb, -nmb);
	//ft_printf("|%8.5u|\t|%8.5u|\t|%08u|\t|%08u|\n",nmb, -nmb, nmb, -nmb);
	//printf("|%08u|", -nmb);
//	ft_printf("|%08u|", -nmb);
	//"|%8.5u|\t|%8.5u|\t|%08u|\t


	//PRINTF("\t|%12i|\t|%12i|\t|%12i|", INT_MIN, INT_MAX,0);
	//int ret = printf("\t|%12i|\t|%12i|\t|%12i|\n", INT_MIN, INT_MAX, 0);
	//int ret2 = ft_printf("\t|%12i|\t|%12i|\t|%12i|\n", INT_MIN, INT_MAX, 0);
//	int ret = printf("%12i\n", INT_MIN);
//	int ret2 = ft_printf("%12i\n", INT_MIN);
	//if (ret != ret2)
	//	printf("ERROR\n");

//	PRINTF("|%8.5"CNV"|\t|%8.5"CNV"|\t|%08"CNV"|\t|%08"CNV"|",nmb, -nmb, nmb, -nmb);
	//printf("|%.U %.0U %0.U %0.0U|\t\t|%U %.2U %2.U %2.2U|\t\n",0,0,0,0,0,0,0,0);
	//ft_printf("|%.U %.0U %0.U %0.0U|\t\t|%U %.2U %2.U %2.2U|\t\n",0,0,0,0,0,0,0,0);
	//ft_printf("|%.U %.0U %0.U %0.0U|\t\t|%U %.2U %2.U %2.2U|\t",0,0,0,0,0,0,0,0);


	//test3();
	//ft_printf("|%3.5U|\t\t|%3.5U|\t|%07.5U|\t|%07.5U|\n",4235, -4235, 4235, -4235);
	//printf("|%3.5U|\t\t|%3.5U|\t|%07.5U|\t|%07.5U|\n",4235, -4235, 4235, -4235);
	//ft_printf("|%3.5U|\t\t|%3.5U|\t|%07.5U|\t|%07.5U|\n",4235, -4235, 4235, -4235);

	//PRINTF("|%3.5"CNV"|\t\t|%3.5"CNV"|\t|%07.5"CNV"|\t|%07.5"CNV"|",4235, -4235, 4235, -4235);
	//PRINTF("|%."CNV" %.0"CNV" %0."CNV" %0.0"CNV"|\t\t|%"CNV" %.2"CNV" %2."CNV" %2.2"CNV" %#."CNV" %#.0"CNV"|\t\t\t\t",0,0,0,0,0,0,0,0,0,0);
	//printf("%2.i\n",0,0,0,0);
	//ft_printf("%2.i %2.2i",0,0,0,0);

	//PRINTF("\t|%12u|\t|%12u|\t|%12u|", INT_MIN, INT_MAX,0);
	//PRINTF("\t|%012i|\t|%-12i|\t|%012i|",-42,42,42);

	//PRINTF(" %0."CNV" %0.0"CNV"|\t\t|%"CNV" %.2"CNV" %2."CNV" %2.2"CNV"|\t\t\t\t",0,0,0,0,0,0,0,0);
	//printf("%12.-6d\n", 42);
	//ft_printf("%12.-6d\n", 42);
	//PRINTF("|%12.8.10.6"CNV"|\t|%20.8.-12.6"CNV"|\t|%20.8.12.-6"CNV"||%20.-5.12.3"CNV"|\t|%10.8.12.6-"CNV"|",42,42,42,42,42);

	//printf("wewfwf %d",-2);
//	ft_printf("wewfwf %d\n",-2);
	//printf("\n%010d\n", -10);
	//ft_printf("%010d\n", -10);
	//printf("%+2$l.5-.3d\n", 657,656);
	//printf("%+5l12-.6d\n",184467);
	//ft_printf("%+5l12-.6d",18446744073709551615);

//	while (1)
//	{
//
//	}
   //	ft_printf("%0# +-d\n",143, 4.7, 200, 20);
	//printf("% +-+#10.0d, %u\n", 9000, -90);
	//printf("%0# +-d", 10, 3);
	//ft_printf("%-+010d", 10);
	//ft_printf("%-+010d", 10);
	/*printf("%-+010d\n", 10);

	printf("%-+010.3d\n", 10);
	printf("%-+10.10d\n", 10);
	printf("%-+010.1d\n", 10);
	printf("%- 010d\n", 10);
	ft_printf("%-+010d\n", 10);
	printf("%+010d\n", 10);
	printf("% 010d\n", 10);*/

	return 0;
}