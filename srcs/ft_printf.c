//
// Created by Serhii Petrenko on 2/10/17.
//

#include "../includes/ft_printf.h"

static void out_token(t_tokens *tokens)
{
	printf("flag -> %s\n",tokens->flags);
	printf("width -> %d\n",tokens->width);
	printf("precision -> %d\n",tokens->precision);
	printf("length -> %s\n",tokens->len);
	printf("specifier -> %c\n",tokens->specifier);
	//printf("str_len -> %d\n\n",tokens->str_count);
}

static t_tokens_list	*get_node(const char **str)
{
	t_tokens_list *ret_node;
	size_t i;
	char *for_parse;

	(*str)++;
	ret_node = (t_tokens_list *)malloc(sizeof(t_tokens_list));
	//for_parse = str_by_sub(*str);
	ret_node->t = parse(str);
	ret_node->next = 0;
	//ret_node->t->str_count = i;
	return (ret_node);
}

static t_tokens_list *get_info_l(const char *str)
{
	t_tokens_list	*start;
	t_tokens_list	*cur;

	while (*str && *str != '%')
		(str)++;
	if (!*str)
		return (0);
	start = get_node(&str);
	if (start)
	{
		cur = start;
		while (*str)
		{
			if (*str == '%')
			{
				cur->next = get_node(&str);
				cur = cur->next;
				//out_token(cur->t);
			}
			(str)++;
		}
	}
	return (start);
}

int					ft_printf(const char *format, ...)
{
	int 		count;
	va_list		va_l;
	t_tokens_list	*start;
	t_tokens_list	*c_start;
	const char	*c_format;

	c_format = format;
	va_start(va_l, format);
	start = get_info_l(c_format);
	c_start = start;
	while (c_start)
	{
		to_string(c_start->t, &va_l);
		c_start = c_start->next;
	}
	va_end(va_l);
	count = uf_print(1, format, start);
	delete_tokens_list(&start);
//	free(start->t->flags);
//	free(start->t->str);
//	free(start->t);
//	free(start);
	return (count);
}