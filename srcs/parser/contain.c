//
// Created by lune on 2/11/17.
//

#include "../../includes/uf_paterns.h"

size_t	contain(char const *str, char symb)
{
	size_t i;

	i = -1;
	while(str[++i])
		if (str[i] == symb)
			return (i + 1);
	return (0);
}

size_t	contain_length(const char *str)
{
	size_t i;

	i = -1;
	while (lenghts[++i][0])
		if (str[0] == lenghts[i][0])
			return (1);
	return (0);
}