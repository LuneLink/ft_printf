//
// Created by lune on 2/11/17.
//

#include "parser.h"

static void	get_flag(t_tokens *tok, const char **flag)
{
	char *tok_flags;

	tok_flags = tok->flags;
	while (*tok_flags)
	{
		if (*tok_flags == **flag)
		{
			(*flag)++;
			tok->str_len++;
			return;
		}
		tok_flags++;
	}
	*tok_flags = **flag;
	*(tok_flags + 1) = 0;
	(*flag)++;
	tok->str_len++;
}

static void	get_length(t_tokens *tok, const char **str)
{
	tok->str_len++;
	if (tok->len[0] == **str)
		tok->len[1] = **str;
	else
	{
		tok->len[0] = (*str)[0];
		tok->len[1] = 0;
	}
	(*str)++;
	/*if (tok->len[0] == **str)
	{
		tok->len[1] = **str;
		tok->str_len++;
		(*str)++;
	}*/
}

static void	idontknow(t_tokens *tok, const char *str)
{
	if(tok->specifier == 'U')
	{
		tok->specifier = 'u';
		tok->len[0] = 'l';
		tok->len[1] = 0;
	}
	else if (tok->specifier == 'p')
	{
		tok->len[0] = 'j';
		tok->specifier = 'x';
		tok->flags[0] = '#';
	}
	else if (tok->specifier == 'S')
	{
		tok->len[0] = 'l';
		tok->specifier = 's';
	}
}

static void	get_specifier(t_tokens *tok, const char **str)
{
	tok->specifier = **str;
	if(tok->specifier == 'i')
			tok->specifier = 'd';
	else if(tok->specifier == 'D')
	{
		tok->specifier = 'd';
		tok->len[0] = 'l';
	}
	else if(tok->specifier == 'O')
	{
		tok->specifier = 'o';
		tok->len[0] = 'l';
	}
	else if(tok->specifier == 'C')
	{
		tok->specifier = 'c';
		tok->len[0] = 'l';
	}
	else
		idontknow(tok, *str);
	(*str)++;
	tok->str_len++;
}

t_tokens	*parse(const char **str)
{
	t_tokens	*tokens;

	tokens = new_tokens();
	while (**str)
	{
		if (contain(flags, **str))
			get_flag(tokens, str);
		else if (ft_isdigit(**str) || **str == '*' || **str == '.')
			digit_parse(str, tokens);
		else if (contain_length(*str))
			get_length(tokens, str);
		else
		{
			get_specifier(tokens, str);
			break;
		}
	}
	return (tokens);
}

//t_tokens	*parse(const char **str)
//{
//	t_tokens	*tokens;
//
//	tokens = new_tokens();
//	while (**str)
//	{
//		if (contain(flags, **str))
//			get_flag(tokens, str);
//		else if (ft_isdigit(**str) || **str == '*' || **str == '.')
//			digit_parse(str, tokens);
//		else if (contain_length(*str))
//			get_length(tokens, str);
//		else if (contain(specifiers, **str))
//		{
//			get_specifier(tokens, str);
//			break;
//		}
//		else
//		{
//			tokens->specifier = **str;
//			(*str)++;
//			break;
//		}
//	}
//	return (tokens);
//}

