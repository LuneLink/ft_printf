//
// Created by lune on 2/11/17.
//

#include "../../includes/ft_printf.h"

size_t get_numb(int *numb, const char *str)
{
	size_t	i;

	i = 0;
	if (ft_isdigit(str[i]))
	{
		*numb = ft_atoi(str);
		while (ft_isdigit(str[i]))
			i++;
	}
	else
		*numb = 0;
	return (i);
}
