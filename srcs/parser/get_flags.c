//
// Created by lune on 2/12/17.
//

#include "../../includes/parser.h"

static void		get_flag(t_tokens *tok, const char **flag)
{
	char *tok_flags;

	tok_flags = tok->flags;
	while (*tok_flags)
	{
		if (*tok_flags == **flag)
		{
			(*flag)++;
			tok->str_len++;
			return;
		}
		tok_flags++;
	}
	*tok_flags = **flag;
	*(tok_flags + 1) = 0;
	(*flag)++;
	tok->str_len++;
}

//size_t		get_flags(t_tokens *tok, const char *str)
//{
//	int i;
//	int j;
//	int k;
//
//	i = strlen(c_flags); // rm
//	tok->flags = (char *)malloc(i + 1);
//	j = 0;
//	while (j < i + 1) // strnew
//		tok->flags[j++] = 0;
//	j = 0;
//	k = 0;
//	while ((i = contain(c_flags, *str)))
//	{
//		if (!contain(tok->flags, c_flags[i - 1]))
//		{
//			tok->flags[j] = c_flags[i - 1];
//			j++;
//		}
//		str++;
//		k++;
//	}
//	j = strlen(tok->flags);
//	tok->str_count += k;
//	return (k); //rm
//}