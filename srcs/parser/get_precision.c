//
// Created by lune on 2/12/17.
//

#include "../../includes/parser.h"

size_t	get_precision(t_tokens *tok, const char *str)
{
	char	*precision;
	size_t	count;

	precision = 0;
	count = 0;
	if (*str == '.')
	{
		str++;
		count++;
		if (*str == '*')
		{
			tok->p_flag = 2;
			count = 1;
		}
		else
		{
			count += get_numb(&(tok->precision), str);
			tok->p_flag = 1;
		}
	}
	return (count);
}