//
// Created by lune on 2/12/17.
//

#include "../../includes/parser.h"

size_t	get_specifier(t_tokens *tok, const char *str)
{
	size_t i;

	if ((i = contain(specifiers, *str)))
	{
		tok->specifier = specifiers[i - 1];
		if(tok->specifier == 'i')
			tok->specifier = 'd';
		if(tok->specifier == 'D')
		{
			tok->specifier = 'd';
			tok->len[0] = 'l';
		}
		return (1);
	}
	return (0);
}