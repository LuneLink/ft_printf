//
// Created by lune on 2/12/17.
//

#include "../../includes/parser.h"

size_t		get_parameter(t_tokens *tok, const char *str)
{
	int i;
	size_t j;

	if (isdigit(*str)) //replace
	{
		i = atoi(str); // replace this
		j = 0;
		while (isdigit(*str)) // rm
		{
			str++;
			j++;
		}
		if (*str == '$')
		{
			tok->param = i;
			//tok->str_count += j + 1;
			return (j + 1);
		}
	}
	return (0);
}