//
// Created by lune on 2/21/17.
//

#include "parser.h"

void	remove_tokens(t_tokens **tokens)
{
	free((*tokens)->flags);
	free(*tokens);
	*tokens = 0;
}