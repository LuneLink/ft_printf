//
// Created by lune on 2/21/17.
//

#include "parser.h"

static void	dot_parse(t_tokens *t, const char **str)
{
	size_t i;

	(*str)++;
	t->str_len++;
	i = get_numb(&(t->precision), *str);
	if (i != 0)
	{
		(*str) += i;
		t->str_len += i;
	}
	else if (**str == '*')
	{
		t->precision = -1;
		t->str_len++;
		(*str)++;
	}
	else
		t->precision = 0;
	t->is_set_pres = 1;
}

static void	width_param_parse(t_tokens *t, const char **str)
{
	size_t	len;
	int 	numb;

	len = get_numb(&numb, *str);
	t->str_len += len;
	(*str) += len;
	if (**str == '$')
	{
		t->param = numb;
		(*str)++;
		t->str_len++;
	}
	else
		t->width = numb;
}

void	digit_parse(const char **str, t_tokens *tokens)
{
	if (**str == '.')
		dot_parse(tokens, str);
	else if (**str == '*')
	{
		tokens->width = -1;
		(*str)++;
	}
	else
		width_param_parse(tokens, str);
}
