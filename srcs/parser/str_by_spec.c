//
// Created by lune on 2/18/17.
//

#include "parser.h"

char	*str_by_sub(const char *str)
{
	char 	*ret_str;
	size_t	i;
	size_t	j;
	size_t	len;

	len = 0;
	i = 0;
	j = 0;
	ret_str = 0;
	while (str[len])
	{
		if (contain(specifiers, str[len]))
			break;
		len++;
	}
	if (str[len])
	{
		ret_str = ft_strnew(len + 1);
		while (j < len + 1)
			ret_str[j] = str[j++];
	}
	return (ret_str);
}
