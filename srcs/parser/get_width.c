//
// Created by lune on 2/12/17.
//

#include "../../includes/parser.h"

size_t	get_width(t_tokens *tok, const char *str)
{
	size_t	count;

	count = 0;
	if (*str == '*')
	{
		tok->w_flag = 2;
		count = 1;
	}
	else
	{
		count += get_numb(&(tok->width), str);
		if (count != 0)
			tok->w_flag = 1;
	}
	return (count);
}
