//
// Created by lune on 2/12/17.
//

#include "../../includes/parser.h"

t_tokens	*new_tokens()
{
	t_tokens *tokens;

	tokens = (t_tokens *)malloc(sizeof(t_tokens));
	tokens->param = 0;
	tokens->is_set_pres = 0;
	tokens->flags = (char *)malloc(ft_strlen(flags) + 1);
	tokens->flags[0] = 0;
	tokens->len[0] = 0;
	tokens->len[1] = 0;
	tokens->len[2] = 0;
	tokens->precision = 1;
	tokens->w_flag = 0;
	tokens->p_flag = 0;
	tokens->specifier = 0;
	tokens->width = 0;
	tokens->str_len = 0;
	tokens->str = 0;
	return (tokens);
}