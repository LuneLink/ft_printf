//
// Created by Serhii Petrenko on 2/23/17.
//

#include "parser.h"

void		delete_tokens_list(t_tokens_list **tokens_list)
{
	if (tokens_list && *tokens_list)
	{
		delete_tokens_list(&((*tokens_list)->next));
		free((*tokens_list)->t->flags);
		free((*tokens_list)->t->str);
		free((*tokens_list)->t);
		free((*tokens_list));
		(*tokens_list) = 0;
	}
}