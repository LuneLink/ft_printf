//
// Created by lune on 2/11/17.
//

#include "ft_printf.h"

int		uf_print(int fd, const char *str, t_tokens_list *list)
{
	size_t	len;
	int		count_printed;
	wint_t	*c_str;

	count_printed = 0;
	while(*str)
	{
		if (*str == '%')
		{
			c_str = list->t->str;
			count_printed += ft_putwstr(c_str);
			//len = ft_wstrlen(c_str);
			//write(fd, c_str, len);
			str += list->t->str_len;
			//count_printed += len;
			list = list->next;
		}
		else
			count_printed += write(fd, str, 1);
		str++;
		//count_printed++;
	}
	return (count_printed);
}

//int		uf_print(int fd, const char *str, t_tokens_list *list)
//{
//	size_t	len;
//	int		count_printed;
//	wint_t	*c_str;
//
//	count_printed = 0;
//	while(*str)
//	{
//		if (*str == '%')
//		{
//			c_str = list->t->str;
//			len = ft_wstrlen(c_str);
//			write(fd, c_str, len);
//			str += list->t->str_len;
//			count_printed += len;
//			list = list->next;
//		}
//		else
//			count_printed += write(fd, str, 1);
//		str++;
//		//count_printed++;
//	}
//	return (count_printed);
//}
