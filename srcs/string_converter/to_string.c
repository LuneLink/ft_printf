//
// Created by Serhii Petrenko on 2/13/17.
//

#include "string_converter.h"

void		to_string(t_tokens *token, va_list *cur_el)
{
	wint_t *ret_str;

	ret_str = 0;
	apply_width_spec(token, cur_el);
	if (token->specifier == 's')
	{
		ret_str = apply_lens(token,cur_el);
		formats(&ret_str, token);
	}
	else if (token->specifier == 'd')
	{
		ret_str = decimal_to_str(token, cur_el);
		formatd(ret_str, token);
	}
	else if (contain(unsigned_d, token->specifier))
	{
		ret_str = udecimal_to_str(token, cur_el);
		formatd(ret_str, token);
	}
	else
		formatc(apply_lenc(token, cur_el), token);
	free(ret_str);
}