//
// Created by Serhii Petrenko on 2/17/17.
//

#include "string_converter.h"

char pop_sign(wint_t **str, char *flags)
{
	if (**str == '-')
	{
		(*str)++;
		return ('-');
	}
	if (contain(flags, '+'))
		return ('+');
	if (contain(flags, ' '))
		return (' ');
	return (0);
}