//
// Created by Serhii Petrenko on 3/1/17.
//

#include "string_converter.h"

static size_t	get_len(char *str)
{
	char shift;
	size_t count;

	count = 0;
	while (*str)
	{
		shift = 7;
		if ((*str >> shift) == 0)
			str++;
		else
		{
			while (((*str >> shift) & 1) == 1)
				shift--;
			str += 7 - shift;
		}
		count++;
	}
	return (count);
}
static wint_t	get_byte(char **utf_symb)
{
	char	shift;
	size_t	count;
	wint_t	symb;
	size_t  i;

	shift = 7;
	count = 0;
	while (((**utf_symb >> shift) & 1) == 1)
	{
		shift--;
		count++;
	}
	symb = (unsigned char)(**utf_symb);
	(*utf_symb)++;
	i = 1;
	while(i < count)
	{
		symb = (wint_t)(symb << 8) | (unsigned char)(**utf_symb);
		i++;
		(*utf_symb)++;
	}
	return (symb);
}

static void		convert_symb(wint_t *symb, char **utf_symb)
{
	*symb = get_byte(utf_symb);
}

wint_t 		*to_wint(char *str)
{
	wint_t	*ret_str;
	size_t 	len;
	size_t  i;

	if (!str)
		return (0);
	len = get_len(str);
	ret_str = ft_wstrnew(len);
	i = 0;
	while (*str)
	{
		convert_symb(&(ret_str[i]), &str);
		i++;
	}
	return (ret_str);
	//0100 0001 1111
}