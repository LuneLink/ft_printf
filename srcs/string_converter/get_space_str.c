//
// Created by Serhii Petrenko on 3/1/17.
//

#include "string_converter.h"

wint_t  *get_space_str(t_tokens *tok, wint_t *str, char symb)
{
	size_t	len;
	size_t	finlen;
	wint_t 	*s_str;

	len  = ft_wstrlen(str);
	if (len < tok->width)
		finlen = tok->width - len;
	else
		return (0);
	s_str = ft_wstrnew(finlen);
	len = -1;
	while (++len < finlen)
		s_str[len] = symb;
	return (s_str);
}