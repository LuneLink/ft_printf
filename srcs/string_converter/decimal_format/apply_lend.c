//
// Created by Serhii Petrenko on 2/13/17.
//

#include "string_converter.h"

intmax_t apply_lend(t_tokens *tok, va_list *val)
{
	if (tok->len == 0)
		return (va_arg(*val, int));
	if (tok->len[0] == 'h')
	{
		if (tok->len[1] == 'h')
			return (va_arg(*val, signed char));
		return (va_arg(*val, short int));
	}
	if (tok->len[0] == 'l')
	{
		if (tok->len[1] == 'l')
			return (va_arg(*val, long long int));
		return (va_arg(*val, long int));
	}
	if (tok->len[0] == 'j')
		return (va_arg(*val, intmax_t));
	if (tok->len[0] == 'z')
		return (va_arg(*val, size_t));
	return (va_arg(*val, int));
}