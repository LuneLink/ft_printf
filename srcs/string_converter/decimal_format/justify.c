//
// Created by lune on 2/22/17.
//

#include "string_converter.h"

wint_t		*right_justify(wint_t *numbs, wint_t *spaces)
{
	size_t	len;
	wint_t	*ret_str;

	len = (ft_wstrlen(numbs) + ft_wstrlen(spaces));

	ret_str = ft_wstrnew(len);
	ret_str += len - 1;
	add_and_nextss(&ret_str, numbs);
	add_and_nextss(&ret_str, spaces);
	return (ret_str + 1);
}

wint_t		*left_justify(wint_t *numbs, wint_t *spaces)
{
	size_t	len;
	wint_t	*ret_str;

	len = (ft_wstrlen(numbs) + ft_wstrlen(spaces));

	ret_str = ft_wstrnew(len);
	add_and_nextbs(&ret_str, numbs);
	add_and_nextbs(&ret_str, spaces);
	return (ret_str - len);
}