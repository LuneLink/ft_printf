//
// Created by Serhii Petrenko on 2/13/17.
//

#include "string_converter.h"

wint_t 		*decimal_to_str(t_tokens *tok,  va_list *val)
{
	intmax_t	numb;

	numb = apply_lend(tok, val);
	return (ft_witoa(numb));
}

wint_t 		*udecimal_to_str(t_tokens *tok,  va_list *val)
{
	uintmax_t	numb;
	wint_t		*ret_str;

	numb = apply_lenu(tok, val);
	if (tok->specifier == 'o')
		ret_str = uf_wuitoa_base(numb, 8, 0);
	else if (tok->specifier == 'X')
		ret_str = uf_wuitoa_base(numb, 16, 1);
	else if (tok->specifier == 'x')
		ret_str = uf_wuitoa_base(numb, 16, 0);
	else
		ret_str = uf_wuitoa_base(numb, 10, 0);
	return (ret_str);
}

wint_t 		*pointer_to_str(va_list *val)
{
	return (uf_wuitoa_base((uintmax_t)va_arg(*val, void*), 16, 0));
}
