//
// Created by Serhii Petrenko on 2/22/17.
//

#include "string_converter.h"

uintmax_t apply_lenu(t_tokens *tok, va_list *val)
{
	if (tok->len == 0)
		return (va_arg(*val, unsigned int));
	if (tok->len[0] == 'h')
	{
		if (tok->len[1] == 'h')
			return (va_arg(*val, unsigned char));
		return (va_arg(*val, unsigned short int));
	}
	if (tok->len[0] == 'l')
	{
		if (tok->len[1] == 'l')
			return (va_arg(*val, unsigned long long int));
		return (va_arg(*val, unsigned long int));
	}
	if (tok->len[0] == 'j')
		return (va_arg(*val, uintmax_t));
	if (tok->len[0] == 'z')
		return (va_arg(*val, size_t));
	return (va_arg(*val, unsigned int));
}