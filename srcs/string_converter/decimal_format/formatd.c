//
// Created by lune on 2/22/17.
//

#include "string_converter.h"

static wint_t  *get_numb_str(t_tokens *tok, wint_t *str, wint_t *zero_f)
{
	int		len;
	int		finlen;
	wint_t	*r_str;
	wint_t 	*start;
	int		s_zerof;

	len = (int)ft_wstrlen(str);
	s_zerof = (int)ft_wstrlen(zero_f);
	finlen = len;
	if (tok->precision > finlen)
		finlen = tok->precision;
	if (contain(tok->flags, '0') && !contain(tok->flags, '-')&&
			tok->precision == 1 && finlen + s_zerof < tok->width)
	{
		//if (tok->width > tok->precision)
			finlen = tok->width - s_zerof;
	}
	r_str = ft_wstrnew((size_t)(finlen + s_zerof));
	start = r_str;
	add_and_nextbs(&start, zero_f);
	add_and_nextb(&start, '0', finlen - len);
	add_and_nextbs(&start, str);
	return (r_str);
}

//static char  *get_numb_str(t_tokens *tok, char *str, char *zero_f)
//{
//	int len;
//	int finlen;
//	int	s_zerof;
//
//	len = (int)ft_strlen(str);
//	s_zerof = (int)ft_strlen(zero_f);
//	if (tok->precision > len)
//		finlen = tok->precision;
//	if ((contain(tok->flags, '0') && !contain(tok->flags, '-')
//	{
//		if (tok->width > finlen + s_zerof)
//			finlen = tok->width - s_zerof;
//	}
//	r_str = ft_strnew((size_t)(finlen + s_zerof));
////	start = r_str;
////	add_and_nextbs(&start, zero_f);
////	add_and_nextb(&start, '0', finlen - len);
////	add_and_nextbs(&start, str);
////	return (r_str);
//}

/*static wint_t  *get_space_str(t_tokens *tok, wint_t *numb, char *zero_f)
{
	size_t	len;
	size_t	finlen;
	size_t 	s_zerof;
	wint_t 	*str;

	//s_zerof = ft_strlen(zero_f);
	len  = ft_wstrlen(numb);
	if (len < tok->width)
		finlen = tok->width - len;
	else
		return (0);
	str = ft_wstrnew(finlen);
	len = -1;
	while (++len < finlen)
		str[len] = ' ';
	return (str);
}*/

static void	get_zerof(wint_t *zer_f, t_tokens *tok, wint_t **str)
{
	zer_f[2] = 0;
	zer_f[1] = 0;
	zer_f[0] = 0;

	if (tok->specifier == 'd')
		zer_f[0] = pop_sign(str, tok->flags);
	else if (contain(tok->flags, '#') && **str != '0')
	{
		if (contain(unsigned_d, tok->specifier) && tok->specifier != 'u')
			zer_f[0] = '0';
		if (tok->specifier == 'X' || tok->specifier == 'x')
			zer_f[1] = tok->specifier;
		if (tok->specifier == 'o')
			tok->precision--;
	}
}

void	formatd(wint_t *str, t_tokens *tok)
{
	wint_t	*numb_s;
	wint_t 	*space_s;
	wint_t 	zer_f[3];

	if (str)
	{
		if (*str == '0' && tok->precision == 0)
			*str = 0;
		get_zerof(zer_f, tok, &str);
		numb_s = get_numb_str(tok, str, zer_f);
		space_s = get_space_str(tok, numb_s, ' ');
		if (contain(tok->flags, '-'))
			tok->str = left_justify(numb_s, space_s);
		else
			tok->str = right_justify(numb_s, space_s);
		free(space_s);
		free(numb_s);
	}
}
//void	formatd(char *str, t_tokens *tok)
//{
//	char	*numb_s;
//	char 	*space_s;
//	char 	zer_f[3];
//
//	if (str)
//		if (*str == '0' && tok->precision == 0)
//			tok->str = 0;
//		else
//		{
//			get_zerof(zer_f, tok, &str);
//			numb_s = get_numb_str(tok, str, zer_f);
//			space_s = get_space_str(tok, numb_s, zer_f);
//			if (contain(tok->flags, '-'))
//			tok->str = left_justify(&numb_s, &space_s);
//			else
//				tok->str = right_justify(&numb_s, &space_s);
//		}
//}