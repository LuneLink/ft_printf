//
// Created by lune on 2/14/17.
//

#include "../../includes/string_converter.h"

void		apply_width_spec(t_tokens *tok, va_list *val)
{
	if (tok->w_flag == 2)
		tok->width = va_arg(*val, int);
	if (tok->p_flag == 2)
		tok->precision = va_arg(*val, int);
}