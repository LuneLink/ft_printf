//
// Created by Serhii Petrenko on 2/13/17.
//

#include "../../includes/string_converter.h"

char *get_decimal(intmax_t numb)
{
	size_t	len;
	char 	*str_numb;
	int		div;

	len = get_count_numb(numb, 10);
	if (numb < 0)
		len++;
	str_numb = (char *)malloc(len + 1);
	if (numb < 0)
		str_numb[0] = '-';
	if (numb == 0)
		str_numb[0] = '0';
	else
	{
		str_numb[len] = 0;
		len--;
		while (numb != 0) {
			div = numb % 10;
			str_numb[len] = (div < 0 ? div * -1 : div) + 48;
			len--;
			numb /= 10;
		}
	}
	return (str_numb);
}