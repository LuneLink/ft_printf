//
// Created by Serhii Petrenko on 2/13/17.
//

#include "../../includes/string_converter.h"

size_t	get_count_numb(long long int numb, int base)
{
	size_t i;
	i = 0;
	if (numb == 0)
		return (1);
	while (numb != 0)
	{
		numb /= base;
		i++;
	}
	return (i);
}