//
// Created by Serhii Petrenko on 3/1/17.
//

#include "string_converter.h"


//static wint_t *convert(unsigned char *str)
//{
//	wint_t *ret_str;
//	size_t i;
//
//	if (str )
//	ret_str = ft_wstrnew(ft_strlen(str));
//	i = 0;
//	while (*str)
//	{
//		ret_str[i] = *str;
//		i++;
//		str++;
//	}
//	return (ret_str);
//}

static wint_t wchar_to_wint(wchar_t c)
{
	wint_t r_symb;

	if (c <= 0x7F)
		r_symb = c;
	else if (c <= 0x7FF)
	{
		r_symb = ((wint_t)((c >> 6) + 0xC0)) << 8;
		r_symb |= (c & 0x3F) + 0x80;
		//putchar((char)((c >> 6) + 0xC0));
		//putchar((char)((c & 0x3F) + 0x80));
	}
	else if (c <= 0xFFFF)
	{
		r_symb = ((wint_t)((c >> 12) + 0xE0)) << 16;
		r_symb |= ((wint_t)((c >> 6) & 0x3F) + 0x80) << 8;
		r_symb |= ((wint_t)((c & 0x3F) + 0x80));
		//putchar((char)((c >> 12) + 0xE0));
		//putchar((char)(((c >> 6) & 0x3F) + 0x80));
		//putchar((char)((c & 0x3F) + 0x80));
	}
	else if (c <= 0x10FFFF)
	{
		r_symb = ((wint_t)((c >> 18) + 0xF0)) << 32;
		r_symb |= ((wint_t)((c >> 12) & 0x3F) + 0x80) << 16;
		r_symb |= ((wint_t)((c >> 6) & 0x3F) + 0x80) << 8;
		r_symb |= ((wint_t)((c & 0x3F) + 0x80));
//		putchar((char)((c >> 18) + 0xF0));
//		putchar((char)(((c >> 12) & 0x3F) + 0x80));
//		putchar((char)(((c >> 6) & 0x3F) + 0x80));
//		putchar((char)((c & 0x3F) + 0x80));
	}
	else
		r_symb = 0;
	return  (r_symb);
}

static wint_t *swchar_to_swint(wchar_t *str)
{
	size_t i;
	wint_t *ret_str;

	i = 0;
	while(str[i])
		i++;
	ret_str = ft_wstrnew(i);
	i = 0;
	while(*str)
	{
		ret_str[i] = wchar_to_wint(*str);
		i++;
		str++;
	}
	return (ret_str);
}

wint_t		*apply_lens(t_tokens *tok, va_list *val)
{
	if (tok->len[0] == 'l')
		return (swchar_to_swint(va_arg(*val, wchar_t*)));
	return (to_wint(va_arg(*val, char*)));
}