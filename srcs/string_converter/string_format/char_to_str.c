//
// Created by lune on 2/26/17.
//

#include "string_converter.h"

wint_t 	*char_to_str(t_tokens *tok, va_list *val)
{
	wint_t symb;
	wint_t *str;
	symb = apply_lenc(tok, val);
	str = (wint_t *)malloc(2 * sizeof(wint_t));
	str[1] = 0;
	str[0] = symb;
	return (str);
}