//
// Created by Serhii Petrenko on 2/27/17.
//

#include "string_converter.h"

static char *get_str(wint_t symb)
{
	char	str_symb[4];
	char			i;
	char 			len;

	str_symb[0] = (char)(symb >> 24);
	str_symb[1] = (char)(symb >> 16);
	str_symb[2] = (char)(symb >> 8);
	str_symb[3] = (char)(symb);
	len = 4;
	while (str_symb[0] == 0)
	{
		i = 1;
		while (i < 4)
		{
			str_symb[i - 1] = str_symb[i];
			i++;
		}
		len--;
	}
	while (str_symb[i])
		i++;
	len = (i < len ? i : len);
	str_symb[len] = 0;
	return strdup(str_symb);
}

void formatc(wint_t symb, t_tokens *tok)
{
	char	*symb_str;
	wint_t 	*str_start;
	size_t	len;


	//symb_str = get_str(symb);
	//len = (size_t)tok->width + ft_strlen(symb_str);
	if (tok->width > 0)
		tok->width -= 1;
	tok->str = ft_wstrnew((size_t)tok->width + 1);
	str_start = tok->str;
	if (contain(tok->flags, '-'))
	{
		//add_and_nextbs(&str_start ,(char)symb);
		add_and_nextb(&str_start , (char)symb, 1);
		add_and_nextb(&str_start , ' ', (size_t)tok->width);
	}
	else
	{
		add_and_nextb(&str_start , ' ', (size_t)tok->width);
		add_and_nextb(&str_start , (char)symb, 1);
		//add_and_nextbs(&str_start ,(char)symb);
	}
	//free(symb_str);
}