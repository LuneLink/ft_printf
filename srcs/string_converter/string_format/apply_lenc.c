//
// Created by lune on 2/26/17.
//

#include "string_converter.h"

wint_t apply_lenc(t_tokens *tok, va_list *val)
{
	if (tok->specifier != 'c')
		return (tok->specifier);
	if (tok->len)
	{
		if (tok->len[0] == 'l')
			return (va_arg(*val, wint_t));
	}
	return (va_arg(*val, int));
}