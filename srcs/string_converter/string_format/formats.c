//
// Created by Serhii Petrenko on 3/1/17.
//

#include "string_converter.h"

static void	set_null(wint_t **str)
{
	*str = ft_wstrnew(7);
	(*str)[0] = '(';
	(*str)[1] = 'n';
	(*str)[2] = 'u';
	(*str)[3] = 'l';
	(*str)[4] = 'l';
	(*str)[5] = ')';
	(*str)[6] = 0;
}

static size_t	get_byte_count(wint_t *str)
{
	size_t i;

	i = 0;
	while(*str)
	{
		if (*str <= 255)
			i++;
		else if (*str <= 65535)
			i += 2;
		else if (*str <= 16777215)
			i += 3;
		else
			i += 4;
		str++;
	}
	return (i);
}
static void		cut(wint_t *str, int byte)
{
	size_t i;

	i = 0;
	while (*str)
	{
		if (*str <= 255)
			i++;
		else if (*str <= 65535)
			i += 2;
		else if (*str <= 16777215)
			i += 3;
		else
			i += 4;
		if (i > byte)
		{
			*str = 0;
			return;
		}
		str++;
	}
}

wint_t *get_spaces(t_tokens *tok, wint_t *str, char symb)
{
	size_t len;
	wint_t *spaces;
	size_t i;

	spaces = 0;
	len = get_byte_count(str);
	if (len < tok->width)
	{
		spaces = ft_wstrnew(tok->width - len);
		i = 0;
		while (i < tok->width - len)
		{
			spaces[i] = symb;
			i++;
		}
	}
	return (spaces);
}

void 		formats(wint_t **str, t_tokens *tok)
{
	wint_t	*s_space;
	char	symb;

	if (*str == 0)
		set_null(str);
	symb = ' ';
	if (tok->is_set_pres)
		cut(*str, tok->precision);
	if (contain(tok->flags, '0') && !contain(tok->flags, '-'))
		symb = '0';
	s_space = get_spaces(tok, *str, symb);
	if (contain(tok->flags, '-'))
		tok->str = left_justify(*str, s_space);
	else
		tok->str = right_justify(*str, s_space);
	free(s_space);
	free(*str);
	*str = 0;
}