//
// Created by Serhii Petrenko on 2/23/17.
//

#ifndef FT_PRINTF_PRINTER_H
# define FT_PRINTF_PRINTER_H

# include "parser.h"
# include "unistd.h"

int		uf_print(int fd, const char *str, t_tokens_list *list);
#endif
