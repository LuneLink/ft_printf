//
// Created by Serhii Petrenko on 2/13/17.
//

#ifndef FT_PRINTF_STRING_CONVERTER_H
# define FT_PRINTF_STRING_CONVERTER_H
# include "parser.h"
# include <stdarg.h>
# include "minlib.h"
# include <stdint.h> // rm



char		*get_decimal(intmax_t numb);
char		*get_udecimal();
intmax_t	apply_lend(t_tokens *tok, va_list *val);
uintmax_t	apply_lenu(t_tokens *tok, va_list *val);
wint_t		apply_lenc(t_tokens *tok, va_list *val);
wint_t		*apply_lens(t_tokens *tok, va_list *val);
void		apply_width_spec(t_tokens *tok, va_list *val);
wint_t  	*get_space_str(t_tokens *tok, wint_t *str, char symb);

wint_t 		*decimal_to_str(t_tokens *tok,  va_list *val);
wint_t 		*udecimal_to_str(t_tokens *tok,  va_list *val);
wint_t 		*pointer_to_str(va_list *val);
wint_t 		*char_to_str(t_tokens *tok, va_list *val);
wint_t 		*to_wint(char *str);
size_t		get_count_numb(long long int numb, int base);
void		to_string(t_tokens *token, va_list *cur_el);
size_t		get_strlen_formatd(t_tokens *tokens, char *num);
char 		pop_sign(wint_t **str, char *flags);
void		formatd(wint_t *str, t_tokens *tok);
void 		formatc(wint_t symb, t_tokens *tok);
void 		formats(wint_t **str, t_tokens *tok);
wint_t		*left_justify(wint_t *numbs, wint_t *spaces);
wint_t		*right_justify(wint_t *numbs, wint_t *spaces);
void		formatu(char *str, t_tokens *tok);

#endif
