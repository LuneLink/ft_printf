//
// Created by lune on 2/12/17.
//

#ifndef FT_PRINTF_PARSER_H
# define FT_PRINTF_PARSER_H

#include <stdlib.h>
#include <string.h>  // rm
#include <ctype.h>  //rm
#include "uf_paterns.h"
#include "minlib.h"

typedef	struct			s_tokens
{
	int					param;
	char 				is_set_pres;
	char				*flags;
	char				w_flag;
	int					width;
	char				p_flag;
	int					precision;
	char				len[3];
	char 				specifier;
	size_t 				str_len;
	wint_t				*str;
}							t_tokens;

typedef struct				s_tokens_list
{
	t_tokens				*t;
	struct s_tokens_list	*next;
}							t_tokens_list;


t_tokens	*parse(const char **str);
t_tokens	*new_tokens();
void		remove_token(t_tokens **token);
size_t		get_numb(int *numb, const char *str);
void		digit_parse(const char **str, t_tokens *tokens);
size_t		get_parameter(t_tokens *tok, const char *str);
//char		get_flag(t_tokens *tok, const char **flag);
size_t		get_width(t_tokens *tok, const char *str);
size_t		get_precision(t_tokens *tok, const char *str);
size_t		get_lenghtf(t_tokens *tok, const char *str);
//size_t	get_specifier(t_tokens *tok, const char *str);
char 		*str_by_sub(const char *str);
void		remove_tokens(t_tokens **tokens);
void		delete_tokens_list(t_tokens_list **tokens_list);

#endif
