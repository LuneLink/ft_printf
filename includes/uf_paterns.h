//
// Created by lune on 2/11/17.
//

#ifndef UF_PATERNS_H
# define UF_PATERNS_H

# include <stdlib.h>

static const char	*flags = "-+ #0'I";
static const char	*specifiers = "DdiuUoOxXfFeEgGaAcCspn%brk";
static const char	*unsigned_d = "oOuUxX";
static const char	lenghts[10][3] = {"hh", "h", "l", "ll", "j", "z", "t", "L", 0};

size_t				contain_length(const char *str);
size_t 				contain(char const *str, char symb);

#endif //FT_PRINTF_UF_PATERNS_H
