//
// Created by Serhii Petrenko on 2/15/17.
//

#ifndef FT_MINLIB_H
# define FT_MINLIB_H

#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>//rm
#include <wchar.h>//rm

char	*ft_strnew(size_t size);
wint_t	*ft_wstrnew(size_t size);
size_t	ft_wstrlen(const wint_t *s);
wint_t 	*uf_wuitoa_base(uintmax_t numb, char base, char up);
wint_t	*ft_witoa(intmax_t n);
int		ft_atoi(const char *str);
void	*ft_memalloc(size_t size);
char	*ft_strdup(const char *s);
void	*ft_memcpy(void *destination, const void *source, size_t count);
size_t	ft_strlen(const char *s);
int 	ft_isdigit(int ch);
int		ft_isspace(int ch);
char	*ft_strcpy(char *destination, const char *source);
char	*ft_strcat(char *destination, const char *source);
char	*ft_strjoin(char const *s1, char const *s2);
void	add_and_nextb(wint_t **str, char symb, size_t count);
void	add_and_nextbs(wint_t **str, wint_t *str2);
void	add_and_nexts(wint_t **str, char symb, size_t count);
void	add_and_nextss(wint_t **str, wint_t *str2);
int 	ft_putwstr(wint_t *str);
int 	ft_putchar(int ch);

#endif
